# veriRAM

Sample project for [ZedBoard](https://www.avnet.com/wps/portal/us/products/avnet-boards/avnet-board-families/zedboard/).

Its focus is:

- learn how to use the RAM and use it to communicate between ARM (with ArchLinux) and FPGA
- learn how to use Revision Control Systems and Vivado

## RAM

### FPGA

- `veriRAM.xpr`
- `veriRAM.srcs/`

### ARM

- `build.zig`
- `src/`

```sh
zig build
```
