`timescale 1ns / 1ps

//
// # EditText
// Read from RAM and show the first byte on LEDs.
// While BTN is pressed, write the values of SWITCHes on RAM.
//
module EditText #(
    parameter unsigned addr_width = 32,
    parameter unsigned data_width = 64
)(
    input clk,
    input reset,
    input BTN,
    input [7:0] SWs,
    output [7:0] LDs,

    output [data_width-1:0] write_data,
    output [addr_width-1:0] write_address,

    input [data_width-1:0] read_data,
    output [addr_width-1:0] read_address,

    output init_axi_write,
    output init_axi_read,

    input write_txn_done,
    input read_txn_done
);
    reg [7:0] lds;
    assign LDs = lds;

    assign write_data[7:0] = SWs[7:0];
    assign write_data[15:8] = { SWs[4], SWs[5], SWs[6], SWs[7],
    SWs[0], SWs[1], SWs[2], SWs[3]};

    assign write_data[data_width-1:16] = {(data_width-16){1'b0}};

    assign write_address = {addr_width{1'b0}};
    assign read_address = {addr_width{1'b0}};

    reg init_write;
    reg init_read;

    assign init_axi_write = init_write;
    assign init_axi_read = init_read;

    reg read = 1'b1;
    reg write;

    reg first_read = 1'b1;
    reg first_write = 1'b1;

    always @(posedge clk) begin
        write <= BTN;

        if (read) begin
            if (first_read | ~reset) begin
                init_read <= 1'b1;
                first_read <= 1'b0;
            end
            else if (init_read) init_read <= 1'b0;
            else if (~init_read & read_txn_done)
                if (write) read <= 1'b0;
                else init_read <= 1'b1;

            if (read_txn_done) lds <= read_data[7:0];

            init_write <= 1'b0;
            first_write <= 1'b1;
        end
        else begin
            if (first_write | ~reset) begin
                init_write <= 1'b1;
                first_write <= 1'b0;
            end
            else if (init_write) init_write <= 1'b0;
            else if (~init_write & write_txn_done) read <= 1'b1;

            init_read <= 1'b0;
            first_read <= 1'b1;
        end
    end

endmodule