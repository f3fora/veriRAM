`timescale 1ns / 1ps

//
// # MemoryDriver
//
module MemoryDriver #(
    // 2^32 = 512mb as ZedBoard
    parameter unsigned addr_width = 32,
    // Consider this as fixed
    parameter unsigned data_width = 64,
    // See `burst`. ideally log2 of data_width.
    parameter unsigned burst_size = 3'b110,
    
    parameter unsigned first_addr = 32'h10000000,

    // Do not change this
    parameter unsigned M_AXI_awaddr_width = addr_width,
    parameter unsigned M_AXI_araddr_width = addr_width,

    parameter unsigned M_AXI_wdata_width = data_width,
    parameter unsigned M_AXI_rdata_width = data_width
)(
    //
    // for User
    //
    input clk,
    // reset (active on low)
    input reset,

    input [data_width-1:0] write_data,
    input [addr_width-1:0] write_address,

    output [data_width-1:0] read_data,
    input [addr_width-1:0] read_address,

    input init_axi_write,
    input init_axi_read,

    output write_txn_done,
    output read_txn_done,

    //
    // # M_AXI for AXI Interface
    //

    //    output M_AXI_awid,
    output [M_AXI_awaddr_width-1:0] M_AXI_awaddr,
    output [7:0] M_AXI_awlen,
    output [2:0] M_AXI_awsize,
    output [1:0] M_AXI_awburst,
    output M_AXI_awlock,
    output [3:0] M_AXI_awcache,
    output [2:0] M_AXI_awprot,
    output [3:0] M_AXI_awqos,
    //    output M_AXI_awuser,
    output M_AXI_awvalid,
    input M_AXI_awready,

    //    output M_AXI_wid,
    output [M_AXI_wdata_width-1:0] M_AXI_wdata,
    output [M_AXI_wdata_width/8-1:0] M_AXI_wstrb,
    output M_AXI_wlast,
    //    output M_AXI_wuser,
    output M_AXI_wvalid,
    input M_AXI_wready,

    //    output M_AXI_bid,
    input [1:0] M_AXI_bresp,
    //    input M_AXI_buser,
    input M_AXI_bvalid,
    output M_AXI_bready,

    //    output M_AXI_arid,
    output [M_AXI_araddr_width-1:0] M_AXI_araddr,
    output [7:0] M_AXI_arlen,
    output [2:0] M_AXI_arsize,
    output [1:0] M_AXI_arburst,
    output M_AXI_arlock,
    output [3:0] M_AXI_arcache,
    output [2:0] M_AXI_arprot,
    output [3:0] M_AXI_arqos,
    //    output M_AXI_aruser,
    output M_AXI_arvalid,
    input M_AXI_arready,

    //    input M_AXI_rid,
    input [M_AXI_rdata_width-1:0] M_AXI_rdata,
    input [1:0] M_AXI_rresp,
    input M_AXI_rlast,
    //    input M_AXI_ruser,
    input M_AXI_rvalid,
    output M_AXI_rready
);

    //
    // # Configuration for AXI
    // https://docs.xilinx.com/v/u/en-US/pg059-axi-interconnect
    //

    // constants
    // Xilinx recommends that AXI4 master devices drive their aw/rsize and aw/rburst outputs. Typically, a master
    // device drives an aw/rsize value that corresponds to its interface data width, unless application requirements
    // dictate otherwise. Typically, a master device drives its aw/rburst output to 0b01, which indicates an incremental
    // (INCR) burst.
    // https://www.allaboutcircuits.com/technical-articles/introduction-to-the-advanced-extensible-interface-axi/
    localparam [2:0] size = burst_size;
    localparam [1:0] burst = 2'b01;
    // Xilinx recommends that master devices drive their aw/rcache outputs to 0b0011 to allow the AXI Interconnect core to pack data while performing width conversion.
    localparam [3:0] cache = 4'b0011;
    //

    reg [M_AXI_awaddr_width-1:0] awaddr;
    assign M_AXI_awaddr = first_addr + awaddr;
    assign M_AXI_awlen = 8'b0;
    assign M_AXI_awsize = size;
    assign M_AXI_awburst = burst;
    assign M_AXI_awlock = 1'b0;
    assign M_AXI_awcache = cache;
    assign M_AXI_awprot =3'b0;
    assign M_AXI_awqos = 4'b0;
    reg awvalid;
    assign M_AXI_awvalid = awvalid;

    reg [M_AXI_wdata_width-1:0] wdata;
    assign M_AXI_wdata = wdata;
    assign M_AXI_wstrb = {(M_AXI_wdata_width/8){1'b1}};
    assign M_AXI_wlast = 1'b0;
    reg wvalid;
    assign M_AXI_wvalid = wvalid;

    reg bready;
    assign M_AXI_bready = bready;

    reg [M_AXI_araddr_width-1:0] araddr;
    assign M_AXI_araddr = first_addr + araddr;
    assign M_AXI_arlen = 8'b0;
    assign M_AXI_arsize = size;
    assign M_AXI_arburst = burst;
    assign M_AXI_arlock = 1'b0;
    assign M_AXI_arcache = cache;
    assign M_AXI_arprot = 3'b0;
    assign M_AXI_arqos = 4'b0;
    reg arvalid;
    assign M_AXI_arvalid = arvalid;

    reg [M_AXI_rdata_width-1:0] rdata;
    assign read_data = rdata;
    reg rready;
    assign M_AXI_rready = rready;

    //
    // # Pulse generator
    // Generate a pulse to initiate AXI transaction @(posedge init_axi_{write,read})
    //
    reg [1:0] old_axi_write;
    reg [1:0] old_axi_read;

    wire init_txn_write;
    wire init_txn_read;
    assign init_txn_write = (~old_axi_write[1]) & old_axi_write[0];
    assign init_txn_read = (~old_axi_read[1]) & old_axi_read[0];

    always @(posedge clk) begin
        if (~reset) begin
            old_axi_write <= 2'b0;
            old_axi_read <= 2'b0;
        end
        else begin
            old_axi_write[1] <= old_axi_write[0];
            old_axi_write[0] <=  init_axi_write;
            old_axi_read[1] <= old_axi_read[0];
            old_axi_read[0] <=  init_axi_read;
        end
    end

    //
    // # Write Address Channel
    //
    reg start_single_write;
    always @(posedge clk) begin
        if (~reset | init_txn_write) awvalid <= 1'b0;
        else if (start_single_write) awvalid <= 1'b1;
        else if (M_AXI_awready & awvalid) awvalid <= 1'b0;
    end

    //
    // # Read Address Channel
    //
    reg start_single_read;
    always @(posedge clk) begin
        if (~reset | init_txn_read) arvalid <= 1'b0;
        else if (start_single_read) arvalid <= 1'b1;
        else if (M_AXI_arready & arvalid) arvalid <= 1'b0;
    end

    //
    // # Write Data Channel
    //
    always @(posedge clk) begin
        if (~reset | init_txn_write) wvalid <= 1'b0;
        else if (start_single_write) wvalid <= 1'b1;
        else if (M_AXI_wready & wvalid) wvalid <= 1'b0;
    end

    //
    // # Read Data Channel
    //
    assign read_data = rdata;
    always @(posedge clk) begin
        if (~reset | init_txn_read) rready <= 1'b0;
        else if (M_AXI_rvalid & ~rready) begin
            rready <= 1'b1;
            rdata <= M_AXI_rdata;
        end
        else if (rready) rready <= 1'b0;
    end

    //
    // # Write Response (B) Channel
    //
    always @(posedge clk) begin
        if (~reset | init_txn_write) bready <= 1'b0;
        else if (M_AXI_bvalid & ~bready) bready <= 1'b1;
        else if (bready) bready <= 1'b0;
    end

    reg write_done;
    always @(posedge clk) begin
        if (~reset | init_txn_write) write_done <= 1'b0;
        else if (M_AXI_bvalid & bready) write_done <= 1'b1;
    end

    reg read_done;
    always @(posedge clk) begin
        if (~reset | init_txn_read) read_done <= 1'b0;
        else if (M_AXI_rvalid & rready) read_done <= 1'b1;
    end

    //
    // # Input Data and Address
    //
    always @(posedge clk) begin
        if (~reset) begin
            awaddr <= {M_AXI_awaddr_width{1'b0}};
            wdata <= {M_AXI_wdata_width{1'b0}};
        end
        else if (init_txn_write) begin
            awaddr <= write_address;
            wdata <= write_data;
        end
        else if (init_txn_read) begin
            araddr <= read_address;
        end
    end

    //
    // # State Machine
    //
    reg write_transaction_done;
    reg read_transaction_done;
    assign write_txn_done = write_transaction_done;
    assign read_txn_done = read_transaction_done;

    reg write_issued;
    reg read_issued;

    // enum
    localparam [1:0] IDLE = 2'b00;
    localparam [1:0] EXEC_WRITE = 2'b01;
    localparam [1:0] EXEC_READ = 2'b10;
    //
    // Status register
    reg [1:0] exec_state;

    always @(posedge clk) begin
        if (~reset) begin
            exec_state <= IDLE;

            start_single_write <= 1'b0;
            start_single_read <= 1'b0;

            write_transaction_done <= 1'b0;
            read_transaction_done <= 1'b0;

            write_issued <= 1'b0;
            read_issued <= 1'b0;
        end
        else begin
            case(exec_state)
                IDLE: begin
                    if (init_txn_write) exec_state <= EXEC_WRITE;
                    else if (init_txn_read) exec_state <= EXEC_READ;
                    else exec_state <= IDLE;
                    write_transaction_done <= 1'b0;
                    read_transaction_done <= 1'b0;
                end
                EXEC_WRITE: begin
                    if (write_done) begin
                        exec_state <= IDLE;
                        write_transaction_done <= 1'b1;
                    end
                    else begin
                        exec_state <= EXEC_WRITE;
                        if (~awvalid & ~wvalid & ~M_AXI_bvalid & ~start_single_write & ~write_issued) begin
                            start_single_write <= 1'b1;
                            write_issued <= 1'b1;
                        end
                        else if (bready) write_issued <= 1'b0;
                        else start_single_write <= 1'b0;
                    end
                end
                EXEC_READ: begin
                    if (read_done) begin
                        exec_state <= IDLE;
                        read_transaction_done <= 1'b1;
                    end
                    else begin
                        exec_state <= EXEC_READ;
                        if (~arvalid & ~start_single_read & ~read_issued) begin
                            start_single_read <= 1'b1;
                            read_issued <= 1'b1;
                        end
                        else if (rready) read_issued <= 1'b0;
                        else start_single_read <= 1'b0;
                    end
                end
                default: begin
                    exec_state <= IDLE;
                    write_transaction_done <= 1'b0;
                    read_transaction_done <= 1'b0;
                end
            endcase
        end
    end

endmodule