const std = @import("std");
const log = std.log;
const cmp = std.cstr.cmp;
const os = std.os;
const mem = std.mem;

const vol = @import("volatile.zig");

const DDR_BASE_ADDR = 0x10000000;
const DDR_MAP_SIZE = 0x10000000;

const show_len = 16;

pub fn main() void {
    // open the memory
    var memfd = os.open("/dev/mem", os.O.RDWR | os.O.SYNC, 0666) catch |err| {
        log.err("Cannot open /dev/mem because {}. Exiting.\n", .{err});
        os.exit(1);
    };
    defer os.close(memfd);
    log.info("/dev/mem opened correctly.", .{});

    // map the memory
    var map_base_addr = vol.mmap(null, DDR_MAP_SIZE, os.PROT.READ | os.PROT.WRITE, os.MAP.SHARED, memfd, DDR_BASE_ADDR) catch |err| {
        log.err("Cannot map memory to user space because {}. Exiting.\n", .{err});
        os.exit(1);
    };
    log.info("Memory mapped correctly.", .{});
    defer vol.munmap(map_base_addr);

    // Read argv
    const argv: [][*:0]const u8 = os.argv;
    loop: for (argv) |arg, i| {
        if ((cmp(arg, "-w") == 0) or (cmp(arg, "--write") == 0)) {

            // Cast argv sentinel-terminated pointer to slice.
            const aus_word = mem.span(argv[i + 1]);
            var word: [show_len]u8 = undefined;
            // Have to be reversed to have the same the same value on PL.
            reverse(u8, word[0..], aus_word);

            // Copy word with maximum #word_len bytes at the beginning of map_base_addr
            vol.copy(u8, map_base_addr, word[0..aus_word.len]);
            log.info("Copied.", .{});

            {
                var r_word: [show_len]u8 = undefined;
                std.mem.copy(u8, r_word[0..], map_base_addr[0..show_len]);
                // As before
                reverse(u8, r_word[0..], r_word[0..]);
                log.info("word {s}", .{r_word});
            }

            break :loop;
        } else if (cmp(arg, "--data") == 0) {
            const foo = Foo{ .a = 0b1, .b = 0b1, .c = 0b1, .d = 0b1 };

            vol.copy(u8, map_base_addr, foo.to_bytes());
            log.info("Copied.", .{});

            {
                var aus_bar: [2]u8 = undefined;
                std.mem.copy(u8, aus_bar[0..], map_base_addr[0..2]);
                const bar = Foo.from_bytes(aus_bar[0..]);
                log.info("data {s}", .{bar});
            }

            break :loop;
        } else if ((cmp(arg, "-c") == 0) or (cmp(arg, "--clean") == 0)) {
            var j: usize = 0;
            while (j < show_len) : (j += 1) {
                map_base_addr[j] = 0;
            }
            log.info("Clean.", .{});
            os.nanosleep(1, 0);

            break :loop;
        } else if ((cmp(arg, "-r") == 0) or (cmp(arg, "--read") == 0)) {
            break :loop;
        }
    }

    {
        log.info("Memory mapped at virtual address: {b:0>8}.", .{map_base_addr[0..show_len].*});
        os.exit(0);
    }
}

const unsigned = std.builtin.Signedness.unsigned;
const Int = std.meta.Int;

const Foo = struct {
    const Self = @This();
    const type_a = u2;
    const size_a = @bitSizeOf(type_a);
    const type_b = u3;
    const size_b = @bitSizeOf(type_b);
    const type_c = u4;
    const size_c = @bitSizeOf(type_c);
    const type_d = u7;
    const size_d = @bitSizeOf(type_d);

    a: type_a,
    b: type_b,
    c: type_c,
    d: type_d,

    const size_tot = size_a + size_b + size_c + size_d;
    const type_tot = Int(unsigned, size_tot);

    const bytes_tot = size_tot / 8;

    fn from_bytes(bytes: []u8) Self {
        var aus_bytes = bytes[0..bytes_tot];

        const bits = @bitReverse(type_tot, @bitCast(type_tot, aus_bytes.*));
        return Self{
            .a = @truncate(type_a, bits >> (size_d + size_c + size_b)),
            .b = @truncate(type_b, bits >> (size_d + size_c)),
            .c = @truncate(type_c, bits >> (size_d)),
            .d = @truncate(type_d, bits),
        };
    }

    fn to_bytes(self: Self) []u8 {
        const bits = @bitReverse(type_tot, //
            (@as(type_tot, self.a) << (size_d + size_c + size_b)) //
        + (@as(type_tot, self.b) << (size_d + size_c)) //
        + (@as(type_tot, self.c) << (size_d)) //
        + (@as(type_tot, self.d)));
        var bytes = @bitCast([bytes_tot]u8, bits)[0..bytes_tot];
        return bytes;
    }
};

fn reverse(comptime T: type, dest: []T, source: []const T) void {
    @setRuntimeSafety(false);
    std.debug.assert(dest.len >= source.len);
    for (source) |s, i|
        dest[i] = @bitReverse(T, s);
}
