//! The present module provides some wrapper around `mmap` and `munmap` for linux.
//! The map is `volatile`, so MMIO can be performed correctly.
//! Moreover a `copy` function for `volatile` is implemented.

const std = @import("std");

const os = std.os;
const mem = std.mem;
const linux = os.linux;
const assert = std.debug.assert;

pub fn mmap(
    ptr: ?[*]align(mem.page_size) u8,
    length: usize,
    prot: u32,
    flags: u32,
    fd: linux.fd_t,
    offset: u64,
) os.MMapError![]align(mem.page_size) volatile u8 {
    const ioffset = @bitCast(i64, offset); // the OS treats this as unsigned
    const rc = linux.mmap(ptr, length, prot, flags, fd, ioffset);
    const err = blk: {
        const err = os.errno(rc);
        if (err == .SUCCESS) return @intToPtr([*]align(mem.page_size) volatile u8, rc)[0..length];
        break :blk err;
    };
    switch (err) {
        .SUCCESS => unreachable,
        .TXTBSY => return error.AccessDenied,
        .ACCES => return error.AccessDenied,
        .PERM => return error.PermissionDenied,
        .AGAIN => return error.LockedMemoryLimitExceeded,
        .BADF => unreachable, // Always a race condition.
        .OVERFLOW => unreachable, // The number of pages used for length + offset would overflow.
        .NODEV => return error.MemoryMappingNotSupported,
        .INVAL => unreachable, // Invalid parameters to mmap()
        .NOMEM => return error.OutOfMemory,
        else => return os.unexpectedErrno(err),
    }
}

pub fn munmap(memory: []align(mem.page_size) const volatile u8) void {
    switch (os.errno(linux.munmap(@ptrCast([*]align(mem.page_size) const u8, memory.ptr), memory.len))) {
        .SUCCESS => return,
        .INVAL => unreachable, // Invalid parameters.
        .NOMEM => unreachable, // Attempted to unmap a region in the middle of an existing mapping.
        else => unreachable,
    }
}

pub fn copy(comptime T: type, dest: []volatile T, source: []const T) void {
    @setRuntimeSafety(false);
    assert(dest.len >= source.len);
    for (source) |s, i|
        dest[i] = s;
}

pub fn bitReverseCopy(comptime T: type, dest: []T, source: []volatile T) void {
    @setRuntimeSafety(false);
    assert(dest.len >= source.len);
    for (source) |s, i|
        dest[i] = @bitReverse(T, s);
}
